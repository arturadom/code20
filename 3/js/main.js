$(document).ready(function(){

	var visible = false;
	$("button#show-menu").click(function(e) {

		if(!visible) {
			$("nav").addClass("visible");
			visible = true;
		} else {
			$("nav").removeClass("visible");
			visible = false;
		}
	});
  
  $("nav > ul > li").mouseover(function(e) {
    $(this).children("ul").addClass("visible");
  });
  $("nav > ul > li").mouseout(function(e) {
    $(this).children("ul").removeClass("visible");
  })
  
})

