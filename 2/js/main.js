$(document).ready(function(){

	var visible = false;
	$("button#show-menu").click(function(e) {
		// $(".nav-in").toggle();
		if(!visible) {
			$(".nav").addClass("visible");
			visible = true;
		} else {
			$(".nav").removeClass("visible");
			visible = false;
		}
	});
  
  $(".nav-in > ul > li").mouseover(function(e) {
    $(this).children("ul").addClass("visible");
  });
  $(".nav-in > ul > li").mouseout(function(e) {
    $(this).children("ul").removeClass("visible");
  })
  
  $(".nav a").click(function(e){
  	e.preventDefault();

  	var our_target = $(this).data("target");
  	var top = $("#"+our_target).offset().top;

  	$("body").animate({
  		scrollTop: top
  	},500,function(){})
  })

})

